import React, {Component} from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { AppLoading } from 'expo'

// helper functions
import utils from '../helpers/utils'

import Deck from '../components/Deck'

import iconPlus from '../src/icon_plus.png'

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#00ced1',
    width: 70,
    height: 70,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    bottom: 10
  },
})

export default class Decks extends Component {
  state = {
    ready: false,
    decks: []
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
  })

  componentDidMount() {
    this.updateData()
  }

  updateData() {
    utils.getDecks().then(
      (decks) => {
        if (!decks) {
          return utils.rebuildDecks()
        }
        return decks
      }
    ).then(
      (decks) => {
        this.setState({
          decks,
          ready: true
        })
      }
    )
  }

  render() {
    const { ready } = this.state

    if (!ready) {
      return <AppLoading/>
    }

    return (
      <View style={{flex:1}}>
        <ScrollView style={{height: 20}}>
          {this.state.decks.map(
            (deck, index) => <Deck updater={() => this.updateData()} key={index} id={deck.id} title={deck.title} number={deck.questions.length}/>
          )}
        </ScrollView>

        <TouchableOpacity
          style={styles.btn}
          onPress={() => this.props.navigation.navigate('AddDeck', {updater:() => this.updateData()})}
        >
          <Image source={iconPlus}/>  
        </TouchableOpacity>
      </View>
    )
  }
}
