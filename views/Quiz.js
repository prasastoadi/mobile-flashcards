import React, {Component} from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import { AppLoading } from 'expo'

import Question from '../components/Question'

import utils from '../helpers/utils'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 25,
    marginHorizontal: 25
  },

  questionContainer: {
    flex: 1
  },

  endContainer: {
    justifyContent: 'center'
  },

  btn: {
    padding: 10,
    width: '100%',
    alignItems: 'center',
    marginTop: 20
  },
  btnSuccess:{
    backgroundColor: '#00ced1',
  },
  btnError:{
    backgroundColor: '#b61827',
  },
  btnText: {
    color: 'white'
  },
  title: {
    textAlign: 'center',
    fontSize: 20
  }
})

export default class DeckDetails extends Component {
  state = {
    deck: {},
    ready: false,
    correct: 0,
    incorrect: 0,
    ended: false,
  }
  static navigationOptions = ({ navigation }) => ({
    title: `Quiz`,
  })

  componentDidMount() {
    this.updateData()
  }

  updateData() {
    utils.getDeck(this.props.navigation.state.params.id).then(
      deck => {
        this.setState({
          deck,
          ready: true,
          questions: deck.questions,
          total: deck.questions.length
        })
      }
    )
  }

  end() {
    utils.clearLocalNotification().then(utils.setLocalNotification)
    this.setState({
      ended: true
    })
  }

  removeQuestion() {
    const { questions } = this.state
    questions.splice(0,1)
    this.setState({
      questions
    })
  }

  handleCorrect() {
    const { questions, correct } = this.state
    if (this.state.questions.length > 1) {
      this.removeQuestion()
    } else {
      this.end()
    }

    this.setState({
      correct: correct + 1
    })
  }

  handleIncorrect() {
    const { questions, incorrect } = this.state
    if (this.state.questions.length > 1) {
      this.removeQuestion()
    } else {
      this.end()
    }

    this.setState({
      incorrect: incorrect + 1
    })
  }

  restartQuiz() {
    this.setState({
      ready: false,
      correct: 0,
      incorrect: 0,
      ended: false
    })

    this.updateData()
  }

  goBack() {
    this.props.navigation.goBack()
  }

  render() {
    const { deck, ready, questions, correct, incorrect, ended, total } = this.state

    if (!ready) {
      return <AppLoading/>
    }

    if (!ended) {
      return (
        <View style={styles.container}>
          <Text>{total - (questions.length-1)} / {total}</Text>
          <View style={styles.questionContainer}>
            <Question question={questions[0].question} answer={questions[0].answer} />
            <TouchableOpacity
              style={[styles.btn, styles.btnSuccess]}
              onPress={() => this.handleCorrect()}
            >
              <Text style={styles.btnText}>Correct</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btn, styles.btnError]}
              onPress={() => this.handleIncorrect()}
            >
              <Text style={styles.btnText}>Incorrect</Text>
            </TouchableOpacity>
          </View>  
        </View>
      )
    }

    if (ended) {
      return (
        <View style={[styles.container, styles.endContainer]}>
          <Text style={styles.title}>The end</Text>
          <Text style={{ textAlign:'center' }}>You answer {correct*100/total}% questions correctly.</Text>
          <TouchableOpacity
            style={[styles.btn, styles.btnSuccess]}
            onPress={() => this.restartQuiz()}
          >
            <Text style={styles.btnText}>Restart Quiz</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.btn, styles.btnSuccess]}
            onPress={() => this.goBack()}
          >
            <Text style={styles.btnText}>Back to Deck</Text>
          </TouchableOpacity>
        </View>
      )
    }
  }
}
