import React, {Component} from 'react'

import { StackNavigator } from 'react-navigation';

import { StyleSheet, Text, View, AsyncStorage, StatusBar, Platform } from 'react-native'
import { Constants } from 'expo'

import { setLocalNotification } from './helpers/utils'

import Decks from './views/Decks'
import DeckDetails from './views/DeckDetails'
import AddCard from './views/AddCard'
import AddDeck from './views/AddDeck'
import Quiz from './views/Quiz'

const Stack = StackNavigator({
  Decks: {
    screen: Decks
  },
  DeckDetails: {
    screen: DeckDetails
  },
  AddCard: {
    screen: AddCard
  },
  Quiz: {
    screen: Quiz
  },
  AddDeck: {
    screen: AddDeck
  }
}, {
  cardStyle: {
  }
})

export default class App extends Component {

  componentDidMount() {
    setLocalNotification()
  }

  render() {
    return (
      <View style={{flex:1}}>
        <StatusBar
          barStyle="light-content"  
        />
        <Stack/>
      </View>
    )
  }
}
